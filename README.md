# DEPRECATED

## This test has been deprecated and is no longer run.

The check-dbus-services test aims to test all services configured as bus
activatable, however such a test is proving to be too naive and requires
significant investment to keep in a passing state.

There are an increasing number of services that have needed to be blacklisted
as they don't pass with reasonable rationale:

* Some require the ability to display a GUI and recieve a response that isn't
  currently viable with the current automated test platform nor is a GUI
  present on many of the image types.
* Some require specific devices to be present for the underlying service to
  start that isn't present in on the test systems.
* Some aren't completely installed and operational in the smaller cut-down
  images.

It has been decided that this test lacks sufficent benefit and significant
overhead and is thus being removed.


Check-dbus-services test

Test dbus services
